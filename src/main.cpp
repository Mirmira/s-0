#include <iostream>
#include <vector>
#include "../include/box.h"
#include "../include/container.h"

using namespace cargo;

int main() {
    Box box1(10, 15, 20, 3.0, 50);
    Box box2(20, 25, 30, 4.0, 60);
    Box box3(30, 35, 40, 5.0, 70);
    Box box4(40, 45, 50, 3.0, 80);

    Container container(30, 40, 50, 60.0);

    container.addBox(box1);
    container.addBox(box2);
    container.addBox(box3);
    container.addBox(box4);

    Box boxes[]{box1};

    /*std::cout << Box::totalValue(boxes, 4) << std::endl;
    std::cout << Box::isSumLWHLessThan(boxes, 4, 300) << std::endl;
    std::cout << Box::getMaxWeightUnderVolume(boxes, 4, 40000000) << std::endl;

    std::cout << box1 << std::endl;*/

    container.eraseBox(2);

    std::cout << container.getBox(2) << std::endl;
    std::cout << container.getAmountOfBoxes() << std::endl;
    std::cout << container.getBoxesWeightSum() << std::endl;
    std::cout << container.getBoxesValuetSum() << std::endl;
}
