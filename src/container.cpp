
#include "../include/container.h"

unsigned long long cargo::Container::addBox(Box box){
    if(getBoxesWeightSum() + box.getWeight() > weightLimit){
        throw std::logic_error("Weight Limit");
    }
    boxes.push_back(box);
    return boxes.size() - 1;
}

void cargo::Container::eraseBox(int index){
    boxes.erase(boxes.begin() + index);
}

int cargo::Container::getAmountOfBoxes(){
    return boxes.size();
}

cargo::Box cargo::Container::getBox(int i){
    if(i > boxes.size() or i < 0){
        throw std::logic_error("Out of range");
    }
    return boxes[i];
}

double cargo::Container::getBoxesWeightSum(){
    double sum = 0;
    for(Box b: boxes){
        sum += b.getWeight();
    }
    return sum;
}

double cargo::Container::getBoxesValuetSum(){
    double sum = 0;
    for(Box b: boxes){
        sum += b.getValue();
    }
    return sum;
}

cargo::Box cargo::Container::operator[](int i) {
    return boxes[i];
}

cargo::Container::Container(int length, int width, int height, double weightLimit) : length(length), width(width),
                                                                                     height(height),
                                                                                     weightLimit(weightLimit) {
    boxes = {};
}
